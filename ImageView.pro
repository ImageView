TEMPLATE        = app
CONFIG          += qt warn_on
INCLUDEPATH     += include/
INCLUDEPATH     += ui/include/
OBJECTS_DIR     = tmp/
UI_HEADERS_DIR  = ui/include
UI_SOURCES_DIR  = ui/src
MOC_DIR         = tmp/
HEADERS         = include/ImageViewWindow.hh include/ImageObject.hh
SOURCES         = main.cc src/ImageViewWindow.cc src/ImageObject.cc
FORMS           = ui/ImageViewWindow.ui
RESOURCES       = ui/ImageView.qrc
TARGET          = imageview

# install
target.path = imageview
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS *.pro
sources.path = .
INSTALLS += target sources
