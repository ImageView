/* Copyright (c) 2007, Pekka Kaitaniemi
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 *
 * 3. The name of the author may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR ROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "ImageViewWindow.hh"

#include <QGraphicsView>
#include <QGraphicsScene>

#include <QList>
#include <QByteArray>

#include <QFileDialog>
#include <QImageReader>

//#include <QGLWidget>

#include <iostream>

ImageViewWindow::ImageViewWindow(QWidget *parent)
{
  setupUi(this);

  //  graphicsView->setViewport(new QGLWidget);

  connect(action_Open, SIGNAL(activated()), SLOT(openImageFile()));
  connect(actionZoom_in, SIGNAL(activated()), SLOT(zoomIn()));
  connect(actionZoom_out, SIGNAL(activated()), SLOT(zoomOut()));
}

void ImageViewWindow::openImageFile()
{
  QList<QByteArray> formats = QImageReader::supportedImageFormats();
  QString filter = tr("All supported types") + " (";
  for(int i = 0; i < formats.count(); ++i) {
    filter = filter + " *." + formats[i];
  }
  filter = filter + ") ";

  loadImage(QFileDialog::getOpenFileName(this, tr("Open File"), "/home", filter));
}

void ImageViewWindow::loadImage(QString fileName)
{
  std::cout <<"loading image: " << fileName.toStdString() << std::endl;
  ImageObject *obj = new ImageObject(fileName);
  obj->setPos(0.0, 0.0);
  QGraphicsScene *scene = new QGraphicsScene();
  //  QGraphicsScene scene = graphicsView->scene();
  //  scene->addText("Hello, world!");
  scene->addItem(obj);
  //  scene->addPixmap(QPixmap::fromImage(QImage(fileName), Qt::OrderedAlphaDither));
  graphicsView->setScene(scene);
  graphicsView->show();

  scene->update();
}

void ImageViewWindow::zoomIn()
{
  graphicsView->scale(1.1, 1.1);
}

void ImageViewWindow:: zoomOut()
{
  graphicsView->scale(0.9, 0.9);
}
